<?php

/**
 * @file
 * Provides Token integration.
 */

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function ymd_date_token_info() {
  if (!\Drupal::hasService('token.entity_mapper')) {
    return;
  }

  $types = [];
  $tokens = [];
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->entityClassImplements(ContentEntityInterface::class)) {
      continue;
    }
    $token_type = \Drupal::service('token.entity_mapper')->getTokenTypeForEntityType($entity_type_id);
    if (empty($token_type)) {
      continue;
    }

    $fields = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type_id);
    foreach ($fields as $field_name => $field) {
      if ($field->getType() != 'ymd_date_field_type') {
        continue;
      }

      $tokens[$token_type . '-' . $field_name]['year_only'] = [
        'name' => t('Year only'),
        'description' => NULL,
        'dynamic' => TRUE,
        'module' => 'ymd_date',
      ];
    }
  }

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function ymd_date_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if (empty($data['field_property'])) {
    return $replacements;
  }

  foreach ($tokens as $token => $original) {

    $list = $data[$data['field_name']];
    if (!$list instanceof FieldItemListInterface) {
      continue;
    }

    $delta = 0;
    $parts = explode(':', $token, 2);

    // Test for a delta as the first part.
    if (isset($parts[1]) && is_numeric($parts[1])) {
      if (count($parts) > 1) {
        $delta = $parts[1];
        $property_name = $parts[0];
      }
      else {
        continue;
      }
    }
    else {
      $property_name = $parts[0];
    }

    if ($property_name != 'year_only') {
      continue;
    }

    if (!isset($list[$delta])) {
      continue;
    }

    $replacements[$original] = substr($list[$delta]->get('value')->getValue(), 0, 4);
  }

  return $replacements;
}
