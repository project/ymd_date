<?php

namespace Drupal\ymd_date\Feeds\Target;

use Drupal\feeds\Feeds\Target\Number;

/**
 * Defines the ymd date field mapper.
 *
 * @FeedsTarget(
 *   id = "ymd_date_field_type",
 *   field_types = {
 *     "ymd_date_field_type",
 *   }
 * )
 */
class YMDDateFieldItem extends Number {

  /**
   * {@inheritdoc}
   */
  protected function prepareValue($delta, array &$values) {
    parent::prepareValue($delta, $values);

    $value = &$values['value'];
    // Remove unnecessary 0 chars from start of the value.
    $value = ltrim($value, '0');
    // Add extra zero chars to the value to ensure it is in the correct format.
    if (!empty($value) && strlen($value) < 8) {
      $values['value'] = str_pad($values['value'], 8, '0');
    }
  }

}
