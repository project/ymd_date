<?php

namespace Drupal\ymd_date\Plugin\views\filter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\NumericFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * YMD date views filter.
 *
 * Even thought dates are stored as strings, the numeric filter is extended
 * because it provides more sensible operators.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("ymd_date")
 */
class Date extends NumericFilter {

  /**
   * Constructs a new Date handler.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DateFormatterInterface $date_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Value is already set up properly, we're just adding our new field to it.
    $options['value']['contains']['date_format']['default'] = 'year_only';

    return $options;
  }

  /**
   * Custom value form.
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);

    if (isset($form['expose_button'])) {
      $description = $this->t('Date must be in YYYYMMDD format.');

      if (isset($form['value']['value'])) {
        $form['value']['value']['#description'] = $description;
      }
      else {
        $form['value']['#description'] = $description;
      }

      if (isset($form['value']['min'])) {
        $form['value']['min']['#description'] = $description;
        $form['value']['max']['#description'] = $description;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    parent::buildExposedForm($form, $form_state);
    // Add select elems to ease the user input.
    $form['value']['#access'] = FALSE;
    $form[$this->options['id']]['#access'] = FALSE;
    $ymd_form_id = $this->options['id'] . '_ymd';
    $form[$ymd_form_id] = [
      '#type' => 'fieldset',
      '#title' => $this->options['expose']['label'],
      '#attributes' => ['class' => ['container-inline']],
      '#tree' => TRUE,
    ];

    $ymd_fields_wrapper = &$form[$ymd_form_id];
    // Define a fieldset wrapper.
    if (!in_array($this->operator, $this->operatorValues(2), TRUE)) {
      // Add year field.
      $ymd_fields_wrapper['year'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Year'),
        '#size' => 10,
      ];

      $date_format = $this->value['date_format'];
      // Add month field.
      if ($date_format === 'year_month' || $date_format === 'year_month_day') {
        $ymd_fields_wrapper['month'] = [
          '#type' => 'select',
          '#title' => $this->t('Month'),
          '#options' => $this->monthOptions(),
        ];
      }

      // Add day field.
      if ($this->value['date_format'] === 'year_month_day') {
        $ymd_fields_wrapper['day'] = [
          '#type' => 'select',
          '#title' => $this->t('Day'),
          '#options' => $this->dayOptions(),
        ];
      }

      // Set default values.
      if ($this->value['value'] !== '') {
        preg_match('@(\d{4})(\d{2})(\d{2})@', $this->value['value'], $match);

        $ymd_fields_wrapper['year']['#default_value'] = $match[1];

        if ($date_format === 'year_month' || $date_format === 'year_month_day') {
          $month_fix = $match[2] === '00' ? '01' : $match[2];
          $ymd_fields_wrapper['month']['#default_value'] = $month_fix;
        }

        if ($this->value['date_format'] === 'year_month_day') {
          $ymd_fields_wrapper['day']['#default_value'] = $match[3];
        }
      }
    }
    else {
      unset($ymd_fields_wrapper['#attributes']);
      $wrapper_field_name = $this->options['id'] . '_wrapper';
      $form[$wrapper_field_name]['#access'] = FALSE;
      // Add min fieldset.
      $ymd_fields_wrapper['min'] = [
        '#type' => 'fieldset',
        '#title' => $form[$wrapper_field_name][$this->options['id']]['min']['#title'],
        '#attributes' => ['class' => ['container-inline']],
      ];
      // Add year field.
      $ymd_fields_wrapper['min']['year'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Year'),
        '#size' => 10,
      ];

      $date_format = $this->value['date_format'];
      // Add month field.
      if ($date_format === 'year_month' || $date_format === 'year_month_day') {
        $ymd_fields_wrapper['min']['month'] = [
          '#type' => 'select',
          '#title' => $this->t('Month'),
          '#options' => $this->monthOptions(),
        ];
      }

      // Add day field.
      if ($this->value['date_format'] === 'year_month_day') {
        $ymd_fields_wrapper['min']['day'] = [
          '#type' => 'select',
          '#title' => $this->t('Day'),
          '#options' => $this->dayOptions(),
        ];
      }

      // Set default values for min date.
      if ($this->value['min'] !== '') {
        preg_match('@(\d{4})(\d{2})(\d{2})@', $this->value['min'], $match_min);
        $ymd_fields_wrapper['min']['year']['#default_value'] = $match_min[1];

        if ($date_format === 'year_month' || $date_format === 'year_month_day') {
          $month_fix = $match_min[2] === '00' ? '01' : $match_min[2];
          $ymd_fields_wrapper['min']['month']['#default_value'] = $month_fix;
        }

        if ($this->value['date_format'] === 'year_month_day') {
          $ymd_fields_wrapper['min']['day']['#default_value'] = $match_min[3];
        }
      }

      // Add max fieldset.
      $ymd_fields_wrapper['max'] = [
        '#type' => 'fieldset',
        '#title' => $form[$wrapper_field_name][$this->options['id']]['max']['#title'],
        '#attributes' => ['class' => ['container-inline']],
      ];
      // Add year field.
      $ymd_fields_wrapper['max']['year'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Year'),
        '#size' => 10,
      ];

      $date_format = $this->value['date_format'];
      // Add month field.
      if ($date_format === 'year_month' || $date_format === 'year_month_day') {
        $ymd_fields_wrapper['max']['month'] = [
          '#type' => 'select',
          '#title' => $this->t('Month'),
          '#options' => $this->monthOptions(),
        ];
      }

      // Add day field.
      if ($this->value['date_format'] === 'year_month_day') {
        $ymd_fields_wrapper['max']['day'] = [
          '#type' => 'select',
          '#title' => $this->t('Day'),
          '#options' => $this->dayOptions(),
        ];
      }

      // Set default values for max date.
      if ($this->value['max'] !== '') {
        preg_match('@(\d{4})(\d{2})(\d{2})@', $this->value['max'], $match_max);

        $ymd_fields_wrapper['max']['year']['#default_value'] = $match_max[1];

        if ($date_format === 'year_month' || $date_format === 'year_month_day') {
          $month_fix = $match_max[2] === '00' ? '01' : $match_max[2];
          $ymd_fields_wrapper['max']['month']['#default_value'] = $month_fix;
        }

        if ($this->value['date_format'] === 'year_month_day') {
          $ymd_fields_wrapper['max']['day']['#default_value'] = $match_max[3];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);

    $form['value']['date_format'] = [
      '#type' => 'radios',
      '#title' => $this->t('Date format'),
      '#options' => [
        'year_only' => $this->t('Year only'),
        'year_month' => $this->t('Year and month'),
        'year_month_day' => $this->t('Year, month and day'),
      ],
      '#default_value' => !empty($this->value['date_format']) ? $this->value['date_format'] : 'year_only',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function valueValidate($form, FormStateInterface $form_state) {
    $values = $form_state->getValue(['options', 'value']);
    $error_msg = $this->t('Date must be in YYYYMMDD format.');

    if (!in_array($this->operator, $this->operatorValues(2), TRUE)) {
      // Remove unnecessary 0 chars from start of the values.
      $value = $values['value'];

      if (!$this->isDateCorrect($value)) {
        if (isset($form['value']['value'])) {
          $field_name = implode('][', $form['value']['value']['#parents']);
          $form_state->setErrorByName($field_name, $error_msg);
        }
        else {
          $field_name = implode('][', $form['value']['#parents']);
          $form_state->setErrorByName($field_name, $error_msg);
        }
      }
    }
    else {
      $min = $values['min'];
      $max = $values['max'];

      if (!$this->isDateCorrect($min)) {
        $field_name = implode('][', $form['value']['min']['#parents']);
        $form_state->setErrorByName($field_name, $error_msg);
      }

      if (!$this->isDateCorrect($max)) {
        $field_name = implode('][', $form['value']['max']['#parents']);
        $form_state->setErrorByName($field_name, $error_msg);
      }
    }
  }

  /**
   * Check if value of YMD date is correct.
   *
   * @param string $value
   *   Value of YMD date field.
   *
   * @return bool
   *   TRUE if value of YMD date is correct, FALSE otherwise.
   */
  private function isDateCorrect(string $value): bool {
    if ($value === '') {
      return TRUE;
    }

    // Remove unnecessary 0 chars from start of the values.
    $value = ltrim($value, '0');

    return strlen($value) === 8 && ctype_digit($value);
  }

  /**
   * Validate the exposed handler form.
   */
  public function validateExposed(&$form, FormStateInterface $form_state) {
    parent::validateExposed($form, $form_state);
    $error_msg = $this->t('Date must be in YYYYMMDD format.');
    $ymd_form_id_org = $this->options['id'];
    $ymd_form_id_new = $ymd_form_id_org . '_ymd';

    if (!in_array($this->operator, $this->operatorValues(2), TRUE)) {
      $ymd_date_items = $form_state->getValue($ymd_form_id_new);
      $is_year_empty = $ymd_date_items['year'] === '';

      if (!$is_year_empty) {
        $ymd_value_new = $this->fixExposedDate($form_state, $ymd_form_id_new);

        if (!$this->isDateCorrect($ymd_value_new)) {
          $form_state->setErrorByName($ymd_form_id_new . '][year', $error_msg);
        }
        else {
          $form_state->setValue($ymd_form_id_org, $ymd_value_new);
        }
      }
    }
    else {
      $ymd_date_items = $form_state->getValue([$ymd_form_id_new, 'min']);
      $is_year_empty = $ymd_date_items['year'] === '';

      if (!$is_year_empty) {
        $ymd_value_new = $this->fixExposedDate($form_state, [
          $ymd_form_id_new,
          'min',
        ]);

        if (!$this->isDateCorrect($ymd_value_new)) {
          $form_state->setErrorByName($ymd_form_id_new . '][min][year', $error_msg);
        }
        else {
          $form_state->setValue([$ymd_form_id_org, 'min'], $ymd_value_new);
        }
      }

      $ymd_date_items = $form_state->getValue([$ymd_form_id_new, 'max']);
      $is_year_empty = $ymd_date_items['year'] === '';

      if (!$is_year_empty) {
        $ymd_value_new = $this->fixExposedDate($form_state, [
          $ymd_form_id_new,
          'max',
        ]);

        if (!$this->isDateCorrect($ymd_value_new)) {
          $form_state->setErrorByName($ymd_form_id_new . '][max][year', $error_msg);
        }
        else {
          $form_state->setValue([$ymd_form_id_org, 'max'], $ymd_value_new);
        }
      }
    }
  }

  /**
   * Fix exposed date to match YYYYMMDD format.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string|array $ymd_date_form_id
   *   Value of YMD date field.
   *
   * @return string
   *   Fixed exposed date.
   */
  private function fixExposedDate(FormStateInterface $form_state, $ymd_date_form_id): string {
    $ymd_date_items = $form_state->getValue($ymd_date_form_id);
    $ymd_value_new = implode('', $ymd_date_items);

    if ($ymd_value_new === '') {
      return $ymd_value_new;
    }

    if (!in_array($this->operator, $this->operatorValues(2), TRUE)) {
      $operators_less = [
        '<',
        '<=',
      ];

      $operators_greater = [
        '>',
        '>=',
      ];

      if (in_array($this->operator, $operators_less)) {
        // Fix value of the month.
        // If month is not set then its value is "00" but it shows January for
        // example on node page, so we have to take these dates into account too
        // .
        $ymd_date_items = $this->getMinItems($ymd_date_items);
      }
      elseif (in_array($this->operator, $operators_greater)) {
        $ymd_date_items = $this->getMaxItems($ymd_date_items);
      }
    }
    elseif ($ymd_date_form_id[1] === 'min') {
      $ymd_date_items = $this->getMinItems($ymd_date_items);
    }
    else {
      $ymd_date_items = $this->getMaxItems($ymd_date_items);
    }

    $ymd_value_new = implode('', $ymd_date_items);

    switch (count($ymd_date_items)) {
      case 1:
        $ymd_value_new .= '0000';

        break;

      case 2;
        $ymd_value_new .= '00';

        break;
    }

    return $ymd_value_new;
  }

  /**
   * Provide operators.
   *
   * @return array
   *   The operators array.
   */
  public function operators(): array {
    $operators = parent::operators();
    // Override equal and not equal methods.
    $operators['=']['method'] = $operators['!=']['method'] = 'opEqual';

    return $operators;
  }

  /**
   * Filters by operator equal.
   *
   * @param object $field
   *   The views field.
   */
  protected function opEqual($field) {
    $operator = $this->operator;
    $value = $this->value['value'];
    $field_name = $this->field . '_ymd';
    $field_items = $this->view->exposed_data[$field_name];
    $field_items_count = count($this->view->exposed_data[$field_name]);

    if ($field_items_count === 3) {
      if ($field_items['month'] === '01') {
        // Create an OR group.
        $or_group_id = $this->query->setWhereGroup('OR');
        $this->query->addWhere($or_group_id, $field, $value, $operator);
        // Replace "01" with "00" in value of the month.
        $value = $field_items['year'] . '00' . $field_items['day'];
        $this->query->addWhere($or_group_id, $field, $value, $operator);
      }
    }
    else {
      $operator = $this->operator === '=' ? 'BETWEEN' : 'NOT BETWEEN';

      if ($field_items_count === 2) {
        $value_max = $field_items['year'] . $field_items['month'] . '31';
      }
      else {
        $value_max = $field_items['year'] . '1231';
      }

      $this->query->addWhere($this->options['group'], $field, [$value, $value_max], $operator);
    }
  }

  /**
   * Utility function to provide month options.
   */
  private function monthOptions() {
    $options = [];
    foreach (range(1, 12) as $month) {
      $options[sprintf('%02s', $month)] = $this->dateFormatter->format(gmmktime(0, 0, 0, $month, 2, 1970), 'custom', 'F', NULL);
    }
    return $options;
  }

  /**
   * Utility function to provide day options.
   */
  private function dayOptions() {
    $options = [];
    foreach (range(1, 31) as $day) {
      $options[sprintf('%02s', $day)] = $day;
    }
    return $options;
  }

  /**
   * Fix items for max date.
   *
   * @param array $ymd_date_items
   *   Date items.
   *
   * @return array
   *   Fixed date items.
   */
  private function getMaxItems(array $ymd_date_items): array {
    $items_count = count($ymd_date_items);

    switch ($items_count) {
      case 3:
        // Fix empty day when "Date format" changes in exposed filter.
        if ($ymd_date_items['day'] === '') {
          $ymd_date_items['day'] = '01';
        }

        break;

      case 2:
        $ymd_date_items['day'] = '31';

        // Fix empty month when "Date format" changes in exposed filter.
        if ($ymd_date_items['month'] === '') {
          $ymd_date_items['month'] = '01';
        }

        break;

      case 1:
        $ymd_date_items['month'] = '12';
        $ymd_date_items['day'] = '31';
        break;
    }

    return $ymd_date_items;
  }

  /**
   * Fix items for min date.
   *
   * @param array $ymd_date_items
   *   Date items.
   *
   * @return array
   *   Fixed date items.
   */
  private function getMinItems(array $ymd_date_items): array {
    $items_count = count($ymd_date_items);

    switch ($items_count) {
      case 3:
        if ($ymd_date_items['month'] === '01') {
          $ymd_date_items['month'] = '00';
        }

        // Fix empty day when "Date format" changes in exposed filter.
        if ($ymd_date_items['day'] === '') {
          $ymd_date_items['day'] = '00';
        }

        break;

      case 2:
        // Fix empty month when "Date format" changes in exposed filter.
        if ($ymd_date_items['month'] === '') {
          $ymd_date_items['month'] = '00';
        }

        break;
    }

    return $ymd_date_items;
  }

}
