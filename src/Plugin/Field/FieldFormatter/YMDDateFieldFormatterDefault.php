<?php

namespace Drupal\ymd_date\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'ymd_date_field_formatter_default' formatter.
 *
 * @FieldFormatter(
 *   id = "ymd_date_field_formatter_default",
 *   module = "ymd_date",
 *   label = @Translation("YMD Date"),
 *   field_types = {
 *     "ymd_date_field_type"
 *   }
 * )
 */
class YMDDateFieldFormatterDefault extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The date format entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateFormatStorage;

  /**
   * The Drupal logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new DateTimeDefaultFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $date_format_storage
   *   The date format entity storage.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The Drupal logger factory service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DateFormatterInterface $date_formatter, EntityStorageInterface $date_format_storage, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->dateFormatter = $date_formatter;
    $this->dateFormatStorage = $date_format_storage;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter'),
      $container->get('entity_type.manager')->getStorage('date_format'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format_type_year_only' => 'html_year',
      'format_type_year_month' => 'html_month',
      'format_type_ymd' => 'html_date',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date, $format_type = 'format_type_ymd') {
    return $this->dateFormatter->format($date->getTimestamp(), $format_type, '', NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $time = new DrupalDateTime();
    $format_types = $this->dateFormatStorage->loadMultiple();
    $options = [];
    foreach ($format_types as $type => $type_info) {
      $format = $this->dateFormatter->format($time->getTimestamp(), $type);
      $options[$type] = $type_info->label() . ' (' . $format . ')';
    }

    $form['format_type_year_only'] = [
      '#type' => 'select',
      '#title' => t('Date format (year only)'),
      '#description' => t("Choose a format for displaying the date with only a year. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date."),
      '#options' => $options,
      '#default_value' => $this->getSetting('format_type_year_only'),
    ];

    $form['format_type_year_month'] = [
      '#type' => 'select',
      '#title' => t('Date format (year and month)'),
      '#description' => t("Choose a format for displaying the date with no day. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date."),
      '#options' => $options,
      '#default_value' => $this->getSetting('format_type_year_month'),
    ];

    $form['format_type_ymd'] = [
      '#type' => 'select',
      '#title' => t('Date format (year, month and day)'),
      '#description' => t("Choose a format for displaying full dates. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date."),
      '#options' => $options,
      '#default_value' => $this->getSetting('format_type_ymd'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $date = new DrupalDateTime();

    $format_types = [
      'format_type_ymd' => t('Year, month and day'),
      'format_type_year_month' => t('Year and month'),
      'format_type_year_only' => t('Year only'),
    ];

    foreach ($format_types as $format_type => $label) {
      $summary[] = t('@format: @display', [
        '@format' => $label,
        '@display' => $this->formatDate($date, $this->getSetting($format_type)),
      ]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $format_type_year_only = $this->getSetting('format_type_year_only');
    $format_type_year_month = $this->getSetting('format_type_year_month');
    $format_type_ymd = $this->getSetting('format_type_ymd');

    $has_month = FALSE;
    $has_day = FALSE;

    foreach ($items as $delta => $item) {
      preg_match('@(\d{4})(\d{2})(\d{2})@', $item->value, $match);

      $values = ['year' => $match[1]];

      if (!empty($match[2]) && $match[2] != '00') {
        $values['month'] = $match[2];
        $has_month = TRUE;
      }

      if (!empty($match[3]) && $match[3] != '00') {
        $values['day'] = $match[3];
        $has_day = TRUE;
      }

      $format_type = $format_type_ymd;
      if (!$has_month && !$has_day) {
        $format_type = $format_type_year_only;
      }
      elseif ($has_month && !$has_day) {
        $format_type = $format_type_year_month;
      }

      $date = NULL;
      try {
        $date = DrupalDateTime::createFromArray($values);
      }
      catch (\Exception $ex) {
        $this->loggerFactory->get('ymd_date')->error($ex->getMessage());
      }

      if ($date instanceof DrupalDateTime) {
        $element[$delta] = [
          '#type' => 'markup',
          '#markup' => $this->dateFormatter->format($date->getTimestamp(), $format_type, '', NULL),
        ];
      }
    }
    return $element;
  }

}
