<?php

namespace Drupal\ymd_date\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'ymd_date_field_widget_default' widget.
 *
 * @FieldWidget(
 *   id = "ymd_date_field_widget_default",
 *   module = "ymd_date",
 *   label = @Translation("YMD Date"),
 *   field_types = {
 *     "ymd_date_field_type"
 *   }
 * )
 */
class YMDDateFieldWidgetDefault extends WidgetBase implements WidgetInterface, ContainerFactoryPluginInterface {

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * The widget settings.
   *
   * @var array
   */
  protected $settings;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a AddressDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, DateFormatterInterface $date_formatter) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $field_settings = $this->getFieldSettings();

    // Determine the beginning year.
    $begin = isset($field_settings['begin_year']) ? $field_settings['begin_year'] : 0;
    if (empty($begin) || !is_numeric($begin)) {
      $begin = 1900;
    }

    // Split up the current value.
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    preg_match('@(\d{4})(\d{2})(\d{2})@', $value, $match);

    // Define a fieldset wrapper.
    $element['ymd_date'] = [
      '#type' => 'fieldset',
      '#title' => $element['#title'],
      '#required' => $element['#required'],
      '#attributes' => ['class' => ['container-inline']],
    ];

    $element['ymd_date']['year'] = [
      '#type' => 'select',
      '#options' => $this->yearOptions($begin),
      '#empty_option' => ['0000' => '--'],
      '#default_value' => isset($match[1]) ? $match[1] : '',
    ];

    $element['ymd_date']['month'] = [
      '#type' => 'select',
      '#options' => $this->monthOptions(),
      '#default_value' => (isset($match[2]) ? $match[2] : ''),
      '#empty_option' => ['00' => '--'],
    ];

    $element['ymd_date']['day'] = [
      '#type' => 'select',
      '#options' => $this->dayOptions(),
      '#default_value' => (isset($match[3]) ? $match[3] : ''),
      '#empty_option' => ['00' => '--'],
    ];

    return $element;
  }

  /**
   * Utility function to provide year options.
   */
  private function yearOptions($begin) {
    $options = [];
    foreach (range($begin, date('Y')) as $year) {
      $options[$year] = $year;
    }
    return $options;
  }

  /**
   * Utility function to provide month options.
   */
  private function monthOptions() {
    $options = [];
    foreach (range(1, 12) as $month) {
      $options[sprintf('%02s', $month)] = $this->dateFormatter->format(gmmktime(0, 0, 0, $month, 2, 1970), 'custom', 'F', NULL);
    }
    return $options;
  }

  /**
   * Utility function to provide day options.
   */
  private function dayOptions() {
    $options = [];
    foreach (range(1, 31) as $day) {
      $options[sprintf('%02s', $day)] = $day;
    }
    return $options;
  }

}
