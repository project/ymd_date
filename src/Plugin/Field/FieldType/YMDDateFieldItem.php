<?php

namespace Drupal\ymd_date\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'ymd_date_field_type' field type.
 *
 * @FieldType(
 *   id = "ymd_date_field_type",
 *   label = @Translation("YMD Date"),
 *   module = "ymd_date",
 *   description = @Translation("Provides a date field which allows year, year/month and year/month/day values."),
 *   default_widget = "ymd_date_field_widget_default",
 *   default_formatter = "ymd_date_field_formatter_default"
 * )
 */
class YMDDateFieldItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'length' => 50,
        ],
      ],
      'indexes' => [
        'value' => [
          'value',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Date value'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (isset($values['ymd_date'])) {
      $values['value'] = $values['ymd_date']['year'] . $values['ymd_date']['month'] . $values['ymd_date']['day'];
    }
    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '' || $value === '00000000';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'begin_year' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['begin_year'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Beginning year'),
      '#default_value' => $this->getSetting('begin_year'),
      '#description' => $this->t('Enter the lowest option for the year selector.'),
      '#size' => 6,
      '#required' => TRUE,
    ];

    return $element;
  }

}
