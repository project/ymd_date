<?php

/**
 * @file
 * Provides views data for the ymd_date module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 */
function ymd_date_field_views_data(FieldStorageConfigInterface $field_storage) {
  return ymd_date_type_field_views_data_helper($field_storage, [], $field_storage->getMainPropertyName());
}

/**
 * Provides Views integration for any ymd_date-based fields.
 *
 * Overrides the default Views data for ymd_date-based fields, adding ymd_date
 * views plugins. Modules defining new ymd_date-based fields may use this
 * function to simplify Views integration.
 *
 * @param \Drupal\field\FieldStorageConfigInterface $field_storage
 *   The field storage config entity.
 * @param array $data
 *   Field view data or views_field_default_views_data($field_storage) if empty.
 * @param string $column_name
 *   The schema column name with the ymd_date value.
 *
 * @return array
 *   The array of field views data with the ymd_date plugin.
 *
 * @see ymd_date_field_views_data()
 */
function ymd_date_type_field_views_data_helper(FieldStorageConfigInterface $field_storage, array $data, $column_name) {
  // @todo This code only covers configurable fields, handle base table fields
  //   in https://www.drupal.org/node/2489476.
  $data = empty($data) ? views_field_default_views_data($field_storage) : $data;
  foreach ($data as $table_name => $table_data) {
    // Set the 'ymd_date' filter type.
    $data[$table_name][$field_storage->getName() . '_' . $column_name]['filter']['id'] = 'ymd_date';
  }

  return $data;
}
